module FunctorStuff where

import Data.Time.Clock

offsetCurrentTime :: NominalDiffTime -> IO UTCTime
offsetCurrentTime offset =
  fmap (addUTCTime (offset * 24 * 3600)) getCurrentTime

-- addUTCTime :: NominalDiffTime -> UTCTime -> UTCTime
-- getCurrentTime :: IO UTCTime
-- fmap :: (UTCTime -> UTCTime) -> IO UTCTime -> IO UTCTime
