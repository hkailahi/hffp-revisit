module ReaderStuff where

import Control.Applicative

-- boop
timesTwo :: Integer -> Integer
timesTwo = (*2)

-- doop
plusTen :: Integer -> Integer
plusTen = (+10)

fn1 :: Integer -> Integer
fn1 = timesTwo . plusTen

-- Uses Functor of functions with partially-applied functions as the functorial context
-- fmap (*2) (+10) x == (*2) ((+10) x)
functorFn1 :: Integer -> Integer
functorFn1 = fmap timesTwo plusTen

-- liftA2 (+) (*2) (+10) x == ((*2) x) + ((+10) x)
applicativeFn1 :: Integer -> Integer
applicativeFn1 = (+) <$> timesTwo <*> plusTen

applicativeFn2 :: Integer -> Integer
applicativeFn2 = liftA2 (+) timesTwo plusTen

monadicFn1 :: Integer -> Integer
monadicFn1 = do
  a <- timesTwo
  b <- plusTen
  return (a + b)

  -- ex.
-- λ: monadicFn1 3 == applicativeFn1 3
-- True
-- λ: applicativeFn1 3 == applicativeFn2 3
-- True
