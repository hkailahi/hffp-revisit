module RandomExample2 where

import Control.Applicative (liftA3)
import Control.Monad (replicateM)
import Control.Monad.Trans.State
import System.Random
import RandomExample

rollDie :: State StdGen Die
rollDie = state $ do
  (n, s) <- randomR (1, 6)
  return (intToDie n, s)

rollDie' :: State StdGen Die
rollDie' = intToDie <$> state (randomR (1, 6))

rollDieThreeTimes' :: State StdGen (Die, Die, Die)
rollDieThreeTimes' = liftA3 (,,) rollDie rollDie rollDie

nDie :: Int -> State StdGen [Die]
nDie n = replicateM n rollDie

rollsToGetTwenty :: StdGen -> Int
rollsToGetTwenty g = go 0 0 g
  where
    go :: Int -> Int -> StdGen -> Int
    go sum count gen
      | sum >= 20 = count
      | otherwise =
        let (die, nextGen) = randomR (1, 6) gen
        in go (sum + die) (count + 1) nextGen

  -- ex.
-- λ: evalState rollDieThreeTimes' (mkStdGen 0)
-- (DieSix,DieSix,DieFour)
-- λ: evalState rollDieThreeTimes' (mkStdGen 0)
-- (DieSix,DieSix,DieFour)
-- λ: evalState rollDieThreeTimes' (mkStdGen 0)
-- (DieSix,DieSix,DieFour)
-- λ: evalState rollDieThreeTimes' (mkStdGen 1)
-- (DieSix,DieFive,DieTwo)

-- λ: evalState (nDie 5) (mkStdGen 0)
-- [DieSix,DieSix,DieFour,DieOne,DieFive]
-- λ: evalState (nDie 5) (mkStdGen 1)
-- [DieSix,DieFive,DieTwo,DieSix,DieFive]

-- λ: rollsToGetTwenty (mkStdGen 0)
-- 5
-- λ: rollsToGetTwenty (mkStdGen 1)
-- 5
-- λ: rollsToGetTwenty (mkStdGen 2)
-- 5
-- λ: rollsToGetTwenty (mkStdGen 3)
-- 5
-- λ: (rollsToGetTwenty . mkStdGen) <$> randomIO
-- 6
-- λ: (rollsToGetTwenty . mkStdGen) <$> randomIO
-- 5
-- λ: (rollsToGetTwenty . mkStdGen) <$> randomIO
-- 6
-- λ: (rollsToGetTwenty . mkStdGen) <$> randomIO
-- 5
