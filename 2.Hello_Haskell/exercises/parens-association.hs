-- 2.6 pg 39

-- #1
fn1a :: Int
fn1a = 8 + 7 * 9

fn1b :: Int
fn1b = (8 + 7) * 9

-- Does fn1a == fn1b? No
checkFn1Equal :: Bool
checkFn1Equal = (fn1a) == (fn1b) -- False

-- #2
perimeter2a :: Int -> Int -> Int
perimeter2a x y = (x * 2) + (y * 2)

perimeter2b :: Int -> Int -> Int
perimeter2b x y = x * 2 + y * 2

-- Does perimeter2a == perimeter2b? Yes
checkPerimEqual :: Int -> Int -> Bool
checkPerimEqual x y = perimeter2a x y == perimeter2b x y -- True

-- #3
fxa :: Float -> Float
fxa x = x / 2 + 9

fxb :: Float -> Float
fxb x = x / (2 + 9)

checkFxEqual :: Float -> Bool
checkFxEqual x = fxa x == fxb x
