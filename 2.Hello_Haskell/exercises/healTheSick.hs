-- 2.7 pg 45

-- #1
-- λ: let area x = 3.14 * (x * x)
-- λ: area 5
-- 78.5

-- #2
-- λ: double x = b * 2
--
-- <interactive>:5:12: error: Variable not in scope: b
-- λ: double x = x * 2

-- #3
x = 7
y = 10

f = x + y
-- λ: f
-- 17
