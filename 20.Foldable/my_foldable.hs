module MyFoldable where

newtype Identity a = Identity a
data Optional a = Nada | Yep a deriving (Show)

instance Foldable Identity where
  foldr f z (Identity x) = f x z
  foldl f z (Identity x) = f z x
  foldMap f (Identity x) = f x

instance Foldable Optional where
  foldr _ z Nada    = z
  foldr f z (Yep x) = f x z

  foldl _ z Nada    = z
  foldl f z (Yep x) = f z x

  foldMap _ Nada    = mempty
  foldMap f (Yep a) = f a

  -- ex
-- λ: foldr (*) 1 (Identity 5)
-- 5
-- λ: foldr (*) 5 (Identity 5)
-- 25
-- λ: let fm = foldMap (*5)
-- λ: import Data.Monoid
-- λ: type PI = Product Integer
-- λ: fm (Identity 100) :: PI
-- Product {getProduct = 500}

-- λ: foldr (+) 1 Nothing
-- 1
-- λ: let fm = foldMap (+1)
-- λ: fm Nothing :: Sum Integer
-- Sum {getSum = 0}
-- λ: foldr (+) 1 (Just 3)
-- 4
-- λ: fm $ Just 3 :: Sum Integer
-- Sum {getSum = 4}

-- λ: foldMap (+1) Nada :: Sum Int
-- Sum {getSum = 0}
-- λ: foldMap (+1) Nada :: Product Int
-- Product {getProduct = 1}
-- λ: foldMap (+1) (Just 1) :: Sum Int
-- Sum {getSum = 2}
-- λ: foldMap (+1) (Just 1) :: Product Int
-- Product {getProduct = 2}
-- λ: foldMap (+5) (Just 1) :: Product Int
-- Product {getProduct = 6}
