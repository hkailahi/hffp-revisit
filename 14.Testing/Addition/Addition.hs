module Addition where

import Test.Hspec
import Test.QuickCheck

sayHello :: IO ()
sayHello = putStrLn "hello!"

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0
  where go n d count
         | n < d     = (count, n)
         | otherwise = go (n - d) d (count + 1)

main :: IO ()
main = hspec $ do
  describe "Addition - unit tests" $ do
    it "1 + 1 is greater than 1" $ do
      (1 + 1) > (1 :: Integer) `shouldBe` True
    it "2 + 2 should be equal to 4" $ do
      2 + 2 `shouldBe` (4 :: Integer)
  describe "Addition - property tests" $ do
    it "x + 1 is always greater than x" $ do
      property $ \x -> x + 1 > (x :: Int)
  describe "Division" $ do
    it "15 divided by 3 is 5" $ do
      dividedBy 15 3 `shouldBe` ((5, 0) :: (Integer, Integer))
    it "22 divided by 5 is 4 remainder 2" $ do
      dividedBy 22 5 `shouldBe` ((4, 2) :: (Integer, Integer))
