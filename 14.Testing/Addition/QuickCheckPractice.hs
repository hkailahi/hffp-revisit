module QCP where

import Test.QuickCheck

trivialInt :: Gen Int
trivialInt = return 1

oneToThree :: Gen Int
oneToThree = elements [1,2,3]

oneToThreeButMoreTwos :: Gen Int
oneToThreeButMoreTwos = elements [1,2,2,2,2,2,2,2,2,2,2,2,2,3]

genBool :: Gen Bool
genBool = choose (False, True)

genBool' :: Gen Bool
genBool' = elements [False, True]

genOrdering :: Gen Ordering
genOrdering = elements [LT, EQ, GT]

genChar :: Gen Char
genChar = elements ['a'..'z']

genTuple :: (Arbitrary a, Arbitrary b) => Gen (a, b)
genTuple = do
  a <- arbitrary
  b <- arbitrary
  return (a, b)

genEither :: (Arbitrary a, Arbitrary b) => Gen (Either a b)
genEither = do
  a <- arbitrary
  b <- arbitrary
  elements [Left a, Right b]

-- 50/50 for Nothing vs Just
genMaybe :: Arbitrary a => Gen (Maybe a)
genMaybe = do
  a <- arbitrary
  elements [Nothing, Just a]

genMaybe' :: Arbitrary a => Gen (Maybe a)
genMaybe' = do
  a <- arbitrary
  frequency [ (1, return Nothing)
            , (3, return (Just a))
            ]

  -- ex.
-- λ: sample trivialInt
-- 1
-- 1
-- 1
-- 1
-- 1
-- 1
-- 1
-- 1
-- 1
-- 1
-- 1
-- λ: sample' trivialInt
-- [1,1,1,1,1,1,1,1,1,1,1]

-- λ: sample' oneToThree
-- [1,1,1,3,2,2,3,3,3,1,2]
-- λ: sample' oneToThreeButMoreTwos
-- [2,2,2,3,2,2,2,2,2,2,2]

-- λ: sample (genTuple :: Gen (Int, Int))
-- (0,0)
-- (1,-2)
-- (2,-4)
-- (-5,3)
-- (-2,2)
-- (-9,-7)
-- (11,4)
-- (6,-13)
-- (5,-8)
-- (7,-3)
-- (4,-2)

-- λ: sample (genMaybe' :: Gen (Maybe Int))
-- Just 0
-- Just 2
-- Just (-1)
-- Nothing
-- Just 1
-- Just (-6)
-- Just (-9)
-- Nothing
-- Just 3
-- Nothing
-- Just (-10)
