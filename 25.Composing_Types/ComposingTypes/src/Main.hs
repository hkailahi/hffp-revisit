{-# LANGUAGE InstanceSigs #-}

module Main where

import Control.Applicative

main :: IO ()
main = do
  putStrLn "hello world"

newtype Identity a =
  Identity { runIdentity :: a}

-- λ: :t id
-- id :: a -> a
-- λ: :k Identity
-- Identity :: * -> *

newtype Compose f g a =
  Compose { getCompose :: f (g a)}
  deriving (Eq, Show)

-- λ: :t (.)
-- (.) :: (b -> c) -> (a -> b) -> a -> c
-- λ: :k Compose
-- Compose :: (* -> *) -> (* -> *) -> * -> *

instance Functor Identity where
  fmap f (Identity a) = Identity (f a)

instance (Functor f, Functor g) => Functor (Compose f g) where
  fmap f (Compose fga) =
    Compose $ (fmap . fmap) f fga

-- λ: Compose [Just 1, Nothing]
-- Compose {getCompose = [Just 1,Nothing]}
-- λ: let xs = [Just (1::Int), Nothing]
-- λ: :t Compose xs
-- Compose xs :: Compose [] Maybe Int
-- λ: fmap (+1) (Compose xs)
-- Compose {getCompose = [Just 2,Nothing]}

newtype One f a =
  One (f a) deriving (Eq, Show)

instance Functor f => Functor (One f) where
  fmap f (One fa) = One $ fmap f fa

-- λ: let ys = One $ [1..10]
-- λ: fmap (+1) ys
-- One [2,3,4,5,6,7,8,9,10,11]

newtype Three f g h a =
  Three (f (g (h a))) deriving (Eq, Show)

instance (Functor f, Functor g, Functor h)
      => Functor (Three f g h) where
  fmap f (Three fgha) =
    Three $ (fmap . fmap . fmap) f fgha

-- λ: let zs = Three $ [[[1..5]]]
-- λ: fmap (*10) zs
-- Three [[[10,20,30,40,50]]]

-- Point is to show how Compose allows us
-- to express arbitrarily nest types
v :: Compose []
             Maybe
             (Compose Maybe [] Integer)
v = Compose [Just (Compose $ Just [1])]

instance (Applicative f, Applicative g)
      => Applicative (Compose f g) where
  pure :: a -> Compose f g a
  pure x = Compose $ (pure . pure) x

  (<*>) :: Compose f g (a -> b)
        -> Compose f g a
        -> Compose f g b
  (Compose f) <*> (Compose a) =
    Compose $ ((<*>) <$> f) <*> a

-- instance (Monad f, Monad g)
--       => Monad (Compose f g) where
--   return = pure
--
--   (>>=) :: Compose f g a
--         -> (a -> Compose f g b)
--         -> (Compose f g b)
--   (>>=) = DOESN'T WORK

instance (Foldable f, Foldable g)
      => Foldable (Compose f g) where
 foldMap f (Compose fga) =
     (foldMap . foldMap) f fga

instance (Traversable f, Traversable g)
      => Traversable (Compose f g) where
  traverse :: Applicative f1
           => (a -> f1 b)
           -> Compose f g a
           -> f1 (Compose f g b)
  traverse f (Compose fga) =
    Compose <$> (traverse . traverse) f fga
