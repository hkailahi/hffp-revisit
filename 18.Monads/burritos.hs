module Burritos where

import Control.Monad

bind :: Monad m => (a -> m b) -> m a -> m b
bind f x = join $ fmap f x

sequencing :: IO ()
sequencing = do
  putStrLn "Hello"
  putStrLn "World"

sequencing' :: IO ()
sequencing' =
  putStrLn "Hello" >>
  putStrLn "World"

sequencing'' :: IO ()
sequencing'' =
  putStrLn "Hello" *>
  putStrLn "World"

binding :: IO ()
binding = do
  name <- getLine
  putStrLn name

binding' :: IO ()
binding' =
  getLine >>= putStrLn

bindingAndSequencing :: IO ()
bindingAndSequencing = do
  putStrLn "Enter your name: "
  name <- getLine
  putStrLn ("Hello " ++ name)

bindingAndSequencing' :: IO ()
bindingAndSequencing' =
  putStrLn "Enter your name: " >>
  getLine >>=
    \name -> putStrLn ("Hello " ++ name)
