module RoundCow where

data Cow = Cow {
      name   :: String
    , age    :: Int
    , weight :: Int
  } deriving (Eq, Show)

nonEmpty :: String -> Maybe String
nonEmpty ""  = Nothing
nonEmpty str = Just str

nonNegative :: Int -> Maybe Int
nonNegative n
  | n >= 0    = Just n
  | otherwise = Nothing

-- if Cow's name is Bess, it must be under 500
weightCheck :: Cow -> Maybe Cow
weightCheck c =
  let w = weight c
      n = name c
  in if n == "Bess" && w > 499
     then Nothing
     else Just c

mkSphericalCow :: String -> Int -> Int -> Maybe Cow
mkSphericalCow name' age' weight' = do
  n <- nonEmpty name'
  a <- nonNegative age'
  w <- nonNegative weight'
  weightCheck (Cow n a w)
