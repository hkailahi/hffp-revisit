module Main where

-- all below exist in base
import Control.Monad (forever)   -- Allows us to execute a function repeatedly - infinitely, or until we cause the program to exit or fail
import Data.Char (toLower)       -- toLower 'A' == 'a' / toLower ':' = ':'
import Data.Maybe (isJust)       -- isJust Nothing == False / isJust (Just 10) == True
import Data.List (intersperse)   -- adds something between each element in a list ie, intersperse ' ' "Blah" == "B l a h"
import System.Exit (exitSuccess) -- to exit successfully, indicates whether or not it was a success so our OS knows whether an error occured
import System.Random (randomRIO)  -- randomRIO (0, 5) could be 4, randomRIO (1, 100) could be 99

newtype WordList = WordList [String] deriving (Show, Eq)

type Discovered = [Maybe Char]
type Guessed   = [Char]
type GameWord = String

data Puzzle = Puzzle GameWord Discovered Guessed

instance Show Puzzle where
  show (Puzzle _ discovered guessed) =
    (intersperse ' ' $
     fmap renderPuzzleChar discovered)
    ++ "\nGuessed so far: " ++ guessed

allWords :: IO WordList
allWords = do
  dict <- readFile "data/dict.txt"
  return $ WordList (lines dict)

minWordLength :: Int
minWordLength = 5

maxWordLength :: Int
maxWordLength = 9

gameWords :: IO WordList
gameWords = do
  (WordList aw) <- allWords
  return $ WordList (filter gameLength aw)
  where gameLength w =
          let l = length (w :: String)
          in    l >= minWordLength
             && l <  maxWordLength

randomWord :: WordList -> IO String
randomWord (WordList wl) = do
  randomIndex <- randomRIO (0, length wl - 1)
  return $ wl !! randomIndex

randomWord' :: IO String
randomWord' = gameWords >>= randomWord

freshPuzzle :: String -> Puzzle
freshPuzzle word =
  Puzzle word (fmap (const (Nothing)) word) []

charInWord :: Puzzle -> Char -> Bool
charInWord (Puzzle word dsc gs) c = elem c word

alreadyGuessed :: Puzzle -> Char -> Bool
alreadyGuessed (Puzzle word dsc gs) c = elem c gs

renderPuzzleChar :: Maybe Char -> Char
renderPuzzleChar (Nothing) = '_'
renderPuzzleChar (Just c)  = c

fillInCharacter :: Puzzle -> Char -> Puzzle
fillInCharacter (Puzzle word filledInSoFar s) c =
  Puzzle word newFilledInSoFar (c : s)
  where zipper guessed wordChar guessChar
          | wordChar == guessed = Just wordChar
          | otherwise           = guessChar
        newFilledInSoFar = zipWith (zipper c) word filledInSoFar

handleGuess :: Puzzle -> Char -> IO Puzzle
handleGuess puzzle guess = do
  putStrLn $ "Your guess was: " ++ [guess]
  case (charInWord puzzle guess
      , alreadyGuessed puzzle guess) of
    (_, True) -> do
      putStrLn "You already guessed that, pick something else!"
      return puzzle
    (True, _) -> do
      putStrLn "This character was in the word, filling in the word accordingly"
      return (fillInCharacter puzzle guess)
    (False, _) -> do
      putStrLn "This character wasn't in in the word, try again."
      return (fillInCharacter puzzle guess)

gameOver :: Puzzle -> IO ()
gameOver (Puzzle wordToGuess _ guessed)
  | (length guessed) > 10 =
    do
      putStrLn "You Lose!"
      putStrLn $ "The word was: " ++ wordToGuess
      exitSuccess
  | otherwise                               =
    return ()

gameWin :: Puzzle -> IO ()
gameWin (Puzzle _ filledInSoFar _)
  | all isJust filledInSoFar =
    do
      putStrLn "You win!"
      exitSuccess
  | otherwise                =
    return ()

runGame :: Puzzle -> IO ()
runGame puzzle = forever $ do
  gameOver puzzle
  gameWin puzzle
  putStrLn $ "Current puzzle is " ++ show puzzle
  putStr "Guess a letter: "
  guess <- getLine
  case guess of
    [c] -> handleGuess puzzle c >>= runGame
    _   -> putStrLn "Your guess must be a single character."


main :: IO ()
main = do
  word <- randomWord'
  let puzzle = freshPuzzle (fmap toLower word)
  runGame puzzle
