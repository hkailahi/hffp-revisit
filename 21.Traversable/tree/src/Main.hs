{-# LANGUAGE InstanceSigs #-}

module Main where

import Data.Monoid

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

data Tree a = Empty
            | Leaf a
            | Node (Tree a) a (Tree a)
              deriving (Eq, Show)

  -- Main examples
-- λ: let jstTree = fmap Just $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- λ: let listJst = foldMap (\x -> [x]) jstTree
-- λ: let jstList = sequenceA listJst
-- λ: jstTree
-- Node (Node (Leaf (Just 6)) (Just 7) (Leaf (Just 2))) (Just 12) Empty
-- λ: listJst
-- [Just 6,Just 7,Just 2,Just 12]
-- λ: jstList
-- Just [6,7,2,12]
  -- traverse == sequenceA . fmap
-- λ: traverse Just [6,7,2,12]
-- Just [6,7,2,12]

instance Functor Tree where
  fmap _ Empty = Empty
  fmap f (Leaf x) = Leaf (f x)
  fmap f (Node left x right) = Node (fmap f left) (f x) (fmap f right)
-- λ: :t fmap
-- fmap :: Functor f => (a -> b) -> f a -> f b
-- λ: fmap (+100) $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- Node (Node (Leaf 106) 107 (Leaf 102)) 112 Empty
-- λ: fmap Just $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- Node (Node (Leaf (Just 6)) (Just 7) (Leaf (Just 2))) (Just 12) Empty

instance Foldable Tree where
  foldMap _ Empty = mempty
  foldMap f (Leaf x) = f x
  foldMap f (Node left x right) =
    foldMap f left `mappend` f x `mappend` foldMap f right
-- λ: :t foldMap
-- foldMap :: (Monoid m, Foldable t) => (a -> m) -> t a -> m
-- λ: foldMap (\x -> [x]) $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- [6,7,2,12]
-- λ: toList $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- [6,7,2,12]
-- λ: maximum $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- 12
-- λ: elem 2 $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- True

-- λ: sumFn = foldr (\a b -> a + b) 0
-- λ: sumFn $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- 27
-- λ: maxFn = foldr (\a b -> if a > b then a else b) 0
-- λ: maxFn $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- 12

instance Traversable Tree where
  traverse _ Empty = pure Empty
  traverse f (Leaf x) = Leaf <$> f x
  traverse f (Node left x right) =
    Node <$> traverse f left <*> f x <*> traverse f right
-- λ: :t traverse
-- traverse
--   :: (Applicative f, Traversable t) => (a -> f b) -> t a -> f (t b)
-- λ: traverse (\x -> Just x) $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- Just (Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty)
-- λ: traverse (\x -> [x]) $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- [Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty]
-- λ: traverse (\x -> Just [x]) $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- Just (Node (Node (Leaf [6]) [7] (Leaf [2])) [12] Empty)
-- λ: traverse (\x -> [Just x]) $ Node (Node (Leaf 6) 7 (Leaf 2)) 12 Empty
-- [Node (Node (Leaf (Just 6)) (Just 7) (Leaf (Just 2))) (Just 12) Empty]

instance Eq a => EqProp (Tree a) where
  (=-=) = eq

instance Arbitrary a => Arbitrary (Tree a) where
  arbitrary = genTree

genTree :: Arbitrary a => Gen (Tree a)
genTree = do
  x <- arbitrary
  left <- genTree
  right <- genTree
  frequency [ (1, return Empty)
            , (2, return $ Leaf x)
            , (2, return $ Node left x right) ]

treeTrigger = undefined :: Tree (Int, Int, [Int])

main :: IO ()
main = do
  putStr "\nTree"
  quickBatch (traversable treeTrigger)
