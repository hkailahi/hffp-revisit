{-# LANGUAGE RankNTypes #-}

module Main where

import Text.Trifecta

stop :: Parser a
stop = unexpected "Stop"

-- read a single character '1'
one :: Parser Char
one = char '1'

-- read a single character '1', then stop
one' :: Parser Char
one' = char '1' >> stop

two = char '2'
three = char '3'

-- read two characters, '1' and '2'
oneTwo :: Parser Char
oneTwo = char '1' >> char '2'

-- read two characters, '1' and '2', then stop
oneTwo' :: Parser Char
oneTwo' = oneTwo >> stop

testParse :: Parser Char -> IO ()
testParse p =
  print $ parseString p mempty "123"

testEOF :: Parser () -> IO ()
testEOF p =
  print $ parseString p mempty "123"

-- string parsers

type S = forall m. CharParsing m => m String

oneS :: S
oneS = string "1"

oneTwoS :: S
oneTwoS = string "12"

oneTwoThreeS :: S
oneTwoThreeS = string "123"

testParse' :: Parser String -> IO ()
testParse' p =
  print $ parseString p mempty "123"


pNL :: [Char] -> IO ()
pNL s = putStrLn ('\n' : s)

main :: IO ()
main = do
  pNL "stop:"
  testParse stop
  pNL "one:"
  testParse one
  pNL "one':"
  testParse one'
  pNL "oneTwo:"
  testParse oneTwo
  pNL "oneTwo':"
  testParse oneTwo'
  pNL "one >> EOF:"
  testEOF (one >> eof)
  pNL "oneTwo >> EOF"
  testEOF (oneTwo >> eof)
  pNL "string \"1\", \"12\", \"123\""
  testParse' (choice [ oneTwoThreeS
                     , oneTwoS
                     , oneS
                     , stop ])
  pNL "char \"1\", \"12\", \"123\""
  testParse (choice [ one >> two >> three
                    , one >> two
                    , one
                    , stop ])
