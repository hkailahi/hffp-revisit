{-# LANGUAGE QuasiQuotes #-}

module Quasi where

import Control.Applicative
import Text.RawString.QQ
import Text.Trifecta

eitherOr :: String
eitherOr = [r|
123
abc
456
def
|]

-- λ: :l src/Quasi.hs
-- [1 of 1] Compiling Quasi            ( src/Quasi.hs, interpreted )
-- Ok, modules loaded: Quasi.
-- λ: eitherOr
-- "\n123\nabc\n456\ndef\n"
-- λ: :l src/Quasi.hs
-- [1 of 1] Compiling Quasi            ( src/Quasi.hs, interpreted )
-- src/Quasi.hs:(10,15)-(15,2): Splicing expression
--     template-haskell-2.11.1.0:Language.Haskell.TH.Quote.quoteExp
--       r
--       "\n\
--       \123\n\
--       \abc\n\
--       \456\n\
--       \def\n"
--   ======>
--     "\n\
--     \123\n\
--     \abc\n\
--     \456\n\
--     \def\n"
-- Ok, modules loaded: Quasi.
