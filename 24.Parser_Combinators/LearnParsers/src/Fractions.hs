{-# LANGUAGE OverloadedStrings #-}

module Text.Fractions where

import Control.Applicative
import Data.Ratio ((%))
import Text.Trifecta

badFraction    = "1/0"
alsoBad        = "10"
shouldWork     = "1/2"
shouldAlsoWork = "2/1"

parseFraction :: Parser Rational
parseFraction = do
  numerator <- decimal
  char '/'
  denominator <- decimal
  return (numerator % denominator)

main :: IO ()
main = do
  let parseFraction' =
        parseString parseFraction mempty
  print $ parseFraction' shouldWork
  print $ parseFraction' shouldAlsoWork
  print $ parseFraction' alsoBad          -- 10
  print $ parseFraction' badFraction      -- 1/0
-- λ: main
-- Success (1 % 2)
-- Success (2 % 1)
-- Failure (ErrInfo {_errDoc = (interactive):1:3: error: unexpected
--     EOF, expected: "/", digit
-- 10<EOF>
--   ^     , _errDeltas = [Columns 2 2]})
-- Success *** Exception: Ratio has zero denominator

virtuousFraction :: Parser Rational
virtuousFraction = do
  numerator <- decimal
  char '/'
  denominator <- decimal
  case denominator of
    0 -> fail "Denominator cannpt be 0"
    _ -> return (numerator % denominator)

testVirtuous :: IO ()
testVirtuous = do
  print $ parseString virtuousFraction mempty badFraction
  print $ parseString virtuousFraction mempty alsoBad
  print $ parseString virtuousFraction mempty shouldWork
  print $ parseString virtuousFraction mempty shouldAlsoWork

parseInteger :: Parser Integer
parseInteger = do
  num <- integer
  eof
  return num

testParseInteger :: IO ()
testParseInteger = do
  print $ parseString parseInteger mempty "a123"
  print $ parseString parseInteger mempty "1a23"
  print $ parseString parseInteger mempty "123a"
  print $ parseString parseInteger mempty "a"
  print $ parseString parseInteger mempty "123"

  -- ex. where 'm' is 'Parser'
-- λ: :t try
-- try :: Parsing m => m a -> m a
-- λ: :t string
-- string :: CharParsing m => String -> m String
-- λ: :t notFollowedBy
-- notFollowedBy :: (Show a, Parsing m) => m a -> m ()

-- :t try $ string "let" <* (notFollowedBy alphaNum :: Parser ())
-- try $ string "let" <* (notFollowedBy alphaNum :: Parser ())
--   :: Parser String
-- λ: parseString (try $ string "let" <* (notFollowedBy alphaNum :: Parser ())) mempty "let"
-- Success "let"
-- λ: parseString (try $ string "let" <* (notFollowedBy alphaNum :: Parser ()) >> eof) mempty "let"
-- Success ()
-- λ: parseString (try $ string "let" <* (notFollowedBy alphaNum :: Parser ())) mempty "le"
-- Failure (ErrInfo {_errDoc = (interactive):1:1: error: expected: "let"
-- le<EOF>
-- ^       , _errDeltas = [Columns 0 0]})
