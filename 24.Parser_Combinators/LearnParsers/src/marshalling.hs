{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Marshalling where

import Control.Applicative
import Data.Aeson
import Data.ByteString.Lazy (ByteString)
import qualified Data.Text as T
import Data.Text (Text)
import Text.RawString.QQ
import Data.Scientific

data NumberOrString =
    NumberNOS Integer
  | StringNOS Text
  deriving (Eq, Show)

instance FromJSON NumberOrString where
  parseJSON (Number i) =
    case floatingOrInteger i of
      (Left _) ->
        fail "Must be integral number"
      (Right integer) ->
        return $ NumberNOS integer
  parseJSON (String s) = return $ StringNOS s
  parseJSON _ =
    fail "NumberOrString must\
         \ be number or string"

dec :: ByteString -> Maybe NumberOrString
dec = decode

eitherDec :: ByteString -> Either String NumberOrString
eitherDec = eitherDecode

main' :: IO ()
main' = do
  print $ dec "blah"
  print $ eitherDec "blah"

sectionJson :: ByteString
sectionJson = [r|
{ "section":  {"host": "wikipedia.org"},
  "whatisit": {"red": "intoothandclaw"}
}
|]

data TestData =
  TestData {
    section :: Host
  , what :: Color
  } deriving (Eq, Show)

instance FromJSON TestData where
  parseJSON (Object v) =
    TestData <$> v .: "section"
             <*> v .: "whatisit"
  parseJSON _ =
    fail "Expectd an object for TestData"

newtype Host = Host String deriving (Eq, Show)

instance FromJSON Host where
  parseJSON (Object v) =
    Host <$> v .: "host"
  parseJSON _ =
    fail "Expected an object for Host"

instance FromJSON Color where
  parseJSON (Object v) =
        (Red <$> v .: "red")
    <|> (Blue <$> v .: "blue")
    <|> (Yellow <$> v .: "yellow")
  parseJSON _ =
    fail "Expected an object for Color"

type Annotation = String

data Color =
    Red Annotation
  | Blue Annotation
  | Yellow Annotation
  deriving (Eq, Show)

main :: IO ()
main = do
  let d :: Maybe TestData
      d = decode sectionJson
  print d


-- λ: decode sectionJson :: Maybe Value
-- Just (Object
--         (fromList
--           [("whatisit",
--               Object (fromList [("red",String "intoothandclaw")])),
--            ("section",
--               Object (fromList [("host",String "wikipedia.org")]))
--           ]
--         ))

-- λ: :set -XOverloadedStrings
-- λ: decode "{\"blue\": \"123\"}" :: Maybe Color
-- Just (Blue "123")
-- λ: :set -XQuasiQuotes
-- λ: decode [r|{"red": "123"}|] :: Maybe Color
-- Just (Red "123")
