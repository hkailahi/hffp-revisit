module AltParsing2 where

import Control.Applicative
import Text.Trifecta
import Quasi

type NumberOrString = Either Integer String

a = "blah"
b = "123"
c = "123blah789"

parseNos :: Parser NumberOrString
parseNos = do
  skipMany (oneOf "\n")
  v <-    (Left <$> integer)
      <|> (Right <$> some letter)
  skipMany (oneOf "\n")
  return v

main :: IO ()
main = do
  let p f i =
        parseString f mempty i
  print $ p (some parseNos) eitherOr
